const fs = require('fs-extra')
const path = require('path')
const program = require('commander')
const { Source, buildSchema } = require('graphql')

program
  .option('--pathIN [value]', 'Path of your graphgl schema file.')
  .option('--pathOUT [value]', 'Path or your gql after generator.')
  .parse(process.argv)

const { pathIN, pathOUT } = program

const typeDef = fs.readFileSync(pathIN)
const source = new Source(typeDef)
const gqlSchema = buildSchema(source)

const addQueryDepthLimit = 100

if (gqlSchema.getQueryType()) {
  let resQuery = ``
  Object.keys(gqlSchema.getQueryType().getFields()).forEach((queryType) => {
    const { query } = generateQuery(queryType, 'Query')
    resQuery += `\n${query}`
  })
  try {
    fs.rmdirSync(pathOUT)
  } catch (e) {}
  try {
    fs.mkdirSync(pathOUT)
  } catch (e) {}
  fs.writeFileSync(path.join(pathOUT, 'query.gql'), resQuery)
}
if (gqlSchema.getMutationType()) {
  let resMutation = ``
  Object.keys(gqlSchema.getMutationType().getFields()).forEach((mutationType) => {
    const { query } = generateQuery(mutationType, 'Mutation')
    resMutation += `\n${query}`
  })
  try {
    fs.rmdirSync(pathOUT)
  } catch (e) {}
  try {
    fs.mkdirSync(pathOUT)
  } catch (e) {}
  fs.writeFileSync(path.join(pathOUT, 'mutation.gql'), resMutation)
}
if (gqlSchema.getSubscriptionType()) {
  let resSubscription = ``
  Object.keys(gqlSchema.getSubscriptionType().getFields()).forEach((subscriptionType) => {
    const { query } = generateQuery(subscriptionType, 'Subscription')
    resSubscription += `\n${query}`
  })
  try {
    fs.rmdirSync(pathOUT)
  } catch (e) {}
  try {
    fs.mkdirSync(pathOUT)
  } catch (e) {}
  fs.writeFileSync(path.join(pathOUT, 'subscription.gql'), resSubscription)
}

function cleanName(name) {
  return name.replace(/[[\]!]/g, '')
}
function generateQuery(curName, curParentType) {
  let query = ''
  const hasArgs = false
  const argTypes = []
  function generateFieldData(name, parentType, parentFields, level) {
    const tabSize = 2
    const field = gqlSchema.getType(parentType).getFields()[name]
    const meta = {
      hasArgs: false,
    }
    let fieldStr = ' '.repeat(level * tabSize) + field.name
    if (field.args && field.args.length) {
      meta.hasArgs = true
      const argsList = field.args
        .reduce((acc, cur) => `${acc}, ${cur.name}: $${cur.name}`, '')
        .substring(2)
      fieldStr += `(${argsList})`
      field.args.forEach((arg) => {
        argTypes.push({
          name: `$${arg.name}`,
          type: arg.type,
        })
      })
    }
    const curTypeName = cleanName(field.type.inspect())
    const curType = gqlSchema.getType(curTypeName)
    if (parentFields.filter(x => x.type === curTypeName).length) {
      return { query: '', meta: {} }
    }
    if (level >= addQueryDepthLimit) {
      return { query: '', meta: {} }
    }
    const innerFields = curType.getFields && curType.getFields()
    let innerFieldsData = null
    if (innerFields) {
      innerFieldsData = Object.keys(innerFields)
        .reduce((acc, cur) => {
          if (
            parentFields.filter(x => x.name === cur && x.type === curTypeName)
              .length
          ) {
            return ''
          }
          const curInnerFieldData = generateFieldData(
            cur,
            curTypeName,
            [...parentFields, { name, type: curTypeName }],
            level + 1,
          )
          const curInnerFieldStr = curInnerFieldData.query
          meta.hasArgs = meta.hasArgs || curInnerFieldData.meta.hasArgs
          if (!curInnerFieldStr) {
            return acc
          }
          return `${acc}\n${curInnerFieldStr}`
        }, '')
        .substring(1)
    }
    if (innerFieldsData) {
      fieldStr += `{\n${innerFieldsData}\n`
      fieldStr += `${' '.repeat(level * tabSize)}}`
    }
    return { query: fieldStr, meta }
  }
  const fieldData = generateFieldData(curName, curParentType, [], 1)
  const argStr = argTypes
    .map(argType => `${argType.name}: ${argType.type}`)
    .join(', ')
  switch (curParentType) {
    case gqlSchema.getQueryType() && gqlSchema.getQueryType().name:
      query += `query ${curName}${argStr ? `(${argStr})` : ''}`
      break
    case gqlSchema.getMutationType() && gqlSchema.getMutationType().name:
      query += `mutation ${curName}${argStr ? `(${argStr})` : ''}`
      break
    case gqlSchema.getSubscriptionType()
      && gqlSchema.getSubscriptionType().name:
      query += `subscription ${curName}${argStr ? `(${argStr})` : ''}`
      break
    default:
      throw new Error('parentType is not one of mutation/query/subscription')
  }
  query += `{\n${fieldData.query}\n}`
  const meta = { ...fieldData.meta }
  meta.hasArgs = hasArgs || meta.hasArgs
  return { query, meta }
}
